using System;
using System.ComponentModel.DataAnnotations;

namespace Bloggerhead.Admin.Models
{
    public class SettingsModel
    {
        [Required]
        [StringLength(100)]
        public string BlogTitle { get; set; }

        [Required]
        [StringLength(255)]
        public string BlogDesc { get; set; }

        [Required]
        [StringLength(512)]
        public string BackgroundImgUrl { get; set; }

        [Required]
        public int InitialPostCount { get; set; }

        [Required]
        public int LoadMorePostsCount { get; set; }
    }
}