using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Bloggerhead.Services;

namespace Bloggerhead.Admin.Controllers
{
    [Area("Admin")]
    public class AdminController : AuthBaseController
    {
        private IPostService postService;

        public AdminController(IHttpContextAccessor context, IUserService userService, IPostService postService) 
            : base(context, userService)
        {
            this.postService = postService;
        }

        public IActionResult Index()
        {
            if(!Authenticated())
            {
                return RedirectToAction("Login", "Auth");
            }

            ViewData["Title"] = "All Posts";
            var posts = postService.GetPostsByDateDesc();
            return View(posts);
        }
    }
}