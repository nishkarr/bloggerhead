using System;
using System.Collections.Generic;
using Bloggerhead.Data;

namespace Bloggerhead.Services
{
    public interface IPostService 
    {
        Post CreatePost(string title, string content, string imgUrl, string status, User user, string[] tags, string blurb, string slug);

        Post GetPost(int id);

        IEnumerable<string> GetTags(Post post);

        void SavePost(Post post);

        IEnumerable<Post> GetPostsByDateDesc();

        IEnumerable<Post> GetPublishedPostsByDateDesc(int afterId = 100000, int limit = 2);

        IEnumerable<Post> GetDraftPostsByDateDesc();
    }
}