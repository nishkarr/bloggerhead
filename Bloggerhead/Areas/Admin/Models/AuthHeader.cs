using System;

namespace Bloggerhead.Admin.Models
{
    public class AuthHeader 
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }
}