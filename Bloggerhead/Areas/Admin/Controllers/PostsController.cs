using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Bloggerhead.Admin.Models;
using Bloggerhead.Services;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Linq;
using System.Text;

namespace Bloggerhead.Admin.Controllers
{
    [Area("Admin")]
    public class PostsController : AuthBaseController
    {
        private AppSettings appSettings;
        private IPostService postService;

        public PostsController(IHttpContextAccessor context, IUserService userService, IOptions<AppSettings> options, IPostService postService) 
            : base(context, userService)
        {
            this.appSettings = options.Value;
            this.postService = postService;
        }

        public IActionResult New()
        {
            if(!Authenticated())
            {
                return RedirectToAction("Login", "Auth");
            }

            var model = new PostModel();

            setViewData("Create");
            return View(model);
        }

        [HttpPost]
        public IActionResult New(PostModel model)
        {
            if(!Authenticated())
            {
                return RedirectToAction("Login", "Auth");
            }

            if(!ModelState.IsValid)
            {
                setViewData("Create");
                return View(model);
            }

            var slug = createSlugFromTitle(model.Title);
            var blurb = extractBlurb(model.Content);
            var post = postService.CreatePost(model.Title, model.Content, model.ImgUrl, model.Status, GetUser(), model.Tags.Split(","), blurb, slug);

            return RedirectToAction("Edit", new { id = post.Id });
        }

        public IActionResult Edit(int id)
        {
            if(!Authenticated())
            {
                return RedirectToAction("Login", "Auth");
            }

            var post = postService.GetPost(id);
            var tagStr = string.Join(',', postService.GetTags(post));

            var model = new PostModel
            {
                Title = post.Title,
                Content = post.Content,
                Id = post.Id,
                ImgUrl = post.ImgUrl,
                Status = post.Published? "Published" : "Draft",
                Tags =  tagStr
            };

            setViewData("Edit");
            return View("New", model);
        }

        [HttpPost]
        public IActionResult Edit(int id, PostModel model)
        {
            if(!ModelState.IsValid)
            {
                setViewData("Edit");
                return View(model);
            }

            var post = postService.GetPost(id);
            post.Title = model.Title;
            post.Content = model.Content;
            post.ImgUrl = model.ImgUrl;
            post.Published = model.Status == "Published";
            post.PublishedDate = DateTime.UtcNow;
            post.Blurb = extractBlurb(model.Content);
            post.Slug = createSlugFromTitle(model.Title);

            postService.SavePost(post); // Oh dear, I need to synchronise the tags here.

            return RedirectToAction("Edit", new { id = post.Id });
        }

        public IActionResult Published()
        {
            if(!Authenticated())
            {
                return RedirectToAction("Login", "Auth");
            }

            ViewData["Title"] = "Published Posts";
            var posts = postService.GetPublishedPostsByDateDesc();
            return View("Index", posts);
        }

        public IActionResult Drafts()
        {
            if(!Authenticated())
            {
                return RedirectToAction("Login", "Auth");
            }

            ViewData["Title"] = "Draft Posts";
            var posts = postService.GetDraftPostsByDateDesc();
            return View("Index", posts);
        }

        private void setViewData(string action)
        {
            var storageAccount = appSettings.AzureStorageAccount;
            var storageAccountKey = appSettings.AzureStorageAccountKey;
            var containerName = appSettings.AzureStorageContainer;

            ViewData["Action"] = action;
            ViewData["AzureContainerUrl"] = $"https://{storageAccount}.blob.core.windows.net/{containerName}";

            ViewBag.Statuses = new List<SelectListItem>
            {
                new SelectListItem {Text = "Draft", Value = "Draft"},
                new SelectListItem {Text = "Published", Value = "Published"},
            };
        }

        public static String sanitise(string input)
        {
            const string removeChars = "?/&^$#@!,:;<>’\'*";
            var sb = new StringBuilder(input.Length);
            foreach (char x in input.Where(c => !removeChars.Contains(c)))
            {
                sb.Append(x);
            }
            return sb.ToString();
        }

        private static string createSlugFromTitle(string title)
        {
            title = sanitise(title);
            if(string.IsNullOrEmpty(title))
            {
                return "";
            }

            if(title.Length > 40)
            {
                return title.Substring(0, 40).ToLower().Replace(" ", "-");
            }

            return title.ToLower().Replace(" ", "-");
        }

        
        private static string extractBlurb(string content)
        {
            if(string.IsNullOrEmpty(content))
            {
                return string.Empty;
            }

            var indexOfBr = content.IndexOf("<br>"); // as a very basic burb extractor - just use the first paragraph;

            if(indexOfBr > 0)
            {
                var firstParagraph = content.Substring(0, indexOfBr - 1);
                return $"{firstParagraph}</div>"; // append a closing div since trix editor wraps the content in divs.
            }

            return content;
        }

    }
}