using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Bloggerhead.Admin.Models;
using Bloggerhead.Services;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Linq;
using System.Text;

namespace Bloggerhead.Admin.Controllers
{
    [Area("Admin")]
    public class SettingsController : AuthBaseController
    {
        private ISettingsService settingsService;
        private AppSettings appSettings;

        public SettingsController(IHttpContextAccessor context, IUserService userService, ISettingsService settingsService, IOptions<AppSettings> options) 
            : base(context, userService)
        {
            this.settingsService = settingsService;
            this.appSettings = options.Value;
        }

        public IActionResult Edit()
        {
            if(!Authenticated())
            {
                return RedirectToAction("Login", "Auth");
            }

            var settings = settingsService.GetSettings();

            var model = new SettingsModel
            {
                BlogTitle = settings.BlogTitle,
                BlogDesc = settings.BlogDesc,
                BackgroundImgUrl = settings.BackgroundImgUrl,
                InitialPostCount = settings.InitialPostCount,
                LoadMorePostsCount = settings.LoadMorePostsCount
            };

            setViewData();
            return View(model);
        }

        [HttpPost]
        public IActionResult Edit(SettingsModel model)
        {
            if(!Authenticated())
            {
                return RedirectToAction("Login", "Auth");
            }

            if(!ModelState.IsValid)
            {
                setViewData();
                return View(model);
            }

            var settings = settingsService.GetSettings();
            settings.BlogTitle = model.BlogTitle;
            settings.BlogDesc = model.BlogDesc;
            settings.BackgroundImgUrl = model.BackgroundImgUrl;
            settings.InitialPostCount = model.InitialPostCount;
            settings.LoadMorePostsCount = model.LoadMorePostsCount;

            settingsService.SaveSettings(settings);

            return RedirectToAction("Edit");
        }


        private void setViewData()
        {
            var storageAccount = appSettings.AzureStorageAccount;
            var storageAccountKey = appSettings.AzureStorageAccountKey;
            var containerName = appSettings.AzureStorageContainer;

            ViewData["AzureContainerUrl"] = $"https://{storageAccount}.blob.core.windows.net/{containerName}";
        }
    }
}