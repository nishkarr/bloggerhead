
use bloggerhead;

if not exists(select 1 from [User] where UserName = 'nishkarr')
    insert into [User] (FirstName, Surname, UserName, [Password], Salt, Email)
    values ('Nishkar', 'Ramautar', 'nishkarr', '4128fa438999665e271936e1b0850b38f4166414c3eb98c29645c645f35a06ca', '9Nuck3fIoVLVEupmTFl5Yg==', 'nishkarr@gmail.com');              
                        -- password: Password10

if not exists(select 1 from Settings)
    insert into Settings (Id, BlogTitle, BlogDesc, BackgroundImgUrl,  InitialPostCount, LoadMorePostsCount)
    values(1, 'Super Awesome Blog.', 'Super Awesome Blog', 'https://bloggerheadstorage01.blob.core.windows.net/img/fabian-grohs-529475-unsplash.png', 3 , 2)

