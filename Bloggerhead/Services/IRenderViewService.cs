using System;
using System.Threading.Tasks;

namespace Bloggerhead.Services
{
    public interface IViewRenderService
    {
        Task<string> RenderToStringAsync(string viewName, object model);
    }
}