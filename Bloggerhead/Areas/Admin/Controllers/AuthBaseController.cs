using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Security.Claims;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Bloggerhead.Models;
using Bloggerhead.Services;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Bloggerhead.Data;

namespace Bloggerhead.Admin.Controllers
{
    public abstract class AuthBaseController : Controller
    {
        protected IHttpContextAccessor context;
        protected IUserService userService;

        protected bool Authenticated() => 
            context.HttpContext.User != null && context.HttpContext.User.Identity.IsAuthenticated;
        
        protected User GetUser() => 
            userService.GetUser(context.HttpContext.User.Identity.Name);

        public AuthBaseController(IHttpContextAccessor context, IUserService userService)
            :base()
        {
            this.userService = userService;
            this.context = context;
        }
    }
}