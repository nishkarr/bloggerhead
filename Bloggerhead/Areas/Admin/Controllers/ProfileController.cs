using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Bloggerhead.Admin.Models;
using Bloggerhead.Services;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Linq;
using System.Text;

namespace Bloggerhead.Admin.Controllers
{
    [Area("Admin")]
    public class ProfileController : AuthBaseController
    {
        public ProfileController(IHttpContextAccessor context, IUserService userService) 
            : base(context, userService)
        {

        }

        public IActionResult Index()
        {
            if(!Authenticated())
            {
                return RedirectToAction("Login", "Auth");
            }

            var user = GetUser();

            var userModel = new UserViewModel{
                Id = user.Id,
                FirstName = user.FirstName,
                Surname = user.Surname,
                EmailAddress = user.Email,
                UserName = user.UserName
            };
            return View(userModel);
        }

        [HttpPost]
        public IActionResult Index(UserViewModel model)
        {
            if(!Authenticated())
            {
                return RedirectToAction("Login", "Auth");
            }

            if(!ModelState.IsValid)
            {
                return View(model);
            }

            var user = GetUser();
            user.FirstName = model.FirstName;
            user.Surname = model.Surname;
            user.Email = model.EmailAddress;
            
            userService.SaveUser(user);

            return RedirectToAction("Index");
        }

    }
}