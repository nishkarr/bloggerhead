﻿using System;
using System.Collections.Generic;

namespace Bloggerhead.Data
{
    public partial class Tag
    {
        public Tag()
        {
            PostTag = new HashSet<PostTag>();
        }

        public int Id { get; set; }
        public string Desc { get; set; }

        public ICollection<PostTag> PostTag { get; set; }
    }
}
