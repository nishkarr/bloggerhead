﻿using System;
using System.Collections.Generic;

namespace Bloggerhead.Data
{
    public partial class Post
    {
        public Post()
        {
            PostTag = new HashSet<PostTag>();
        }

        public int Id { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public string ImgUrl { get; set; }
        public int? UserId { get; set; }
        public bool Published { get; set; }
        public DateTime? PublishedDate { get; set; }
        public string Blurb { get; set; }
        public string Slug { get; set; }

        public User User { get; set; }
        public ICollection<PostTag> PostTag { get; set; }
    }
}
