# bloggerhead

A blog engine running on ASP.Net Core MVC and SQL Server: [Bloggerhead](https://bloggerhead.azurewebsites.net)

## Running the app

You will require:

1. a SQL Server Database, the schema and initialisation scripts can be found in the _Database_ directory.
2. You will need an Azure account with the following:
..* An Azure Storage Account
..* A storage container within that account configured with *Anonymous Read Access* Only.
3. You will need to provide the Storage Account Name, Storage Account Key and container name in _appsettings.json_. These will be used to generate authorsied PUT requests to the Azure Storage Container
