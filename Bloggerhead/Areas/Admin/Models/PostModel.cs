using System;
using System.ComponentModel.DataAnnotations;

namespace Bloggerhead.Admin.Models
{
    public class PostModel
    {
        [Required]
        public int Id { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        public string Content { get; set; }
        [Required]
        public string ImgUrl { get; set; }
        public string Tags { get; set; }
        [Required]
        public string Status { get; set; }
    }
}