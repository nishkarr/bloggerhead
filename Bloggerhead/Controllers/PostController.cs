using System.Text;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Bloggerhead.Models;
using Bloggerhead.Services;

namespace Bloggerhead.Controllers
{
    public class PostController : Controller
    {
        private IPostService postService;
        private IViewRenderService viewRenderService;
        private ISettingsService settingsService;

        public PostController(IPostService postService, IViewRenderService viewRenderService, ISettingsService settingsService)
        {
            this.postService = postService;
            this.viewRenderService = viewRenderService;
            this.settingsService = settingsService;
        }

        public IActionResult View(int id)
        {
            var model = new ViewPostModel
            {
                Post = postService.GetPost(id),
                Settings = settingsService.GetSettings()
            };

            return View(model);
        }

        public async Task<JsonResult> LoadMore(int beforePostId, int limit)
        {
            var lastPostId = beforePostId;
            var renderedPosts = new List<string>();

            var posts = postService.GetPublishedPostsByDateDesc(beforePostId, limit);

            foreach(var post in posts)
            {
                lastPostId = post.Id;
                var rendered = await viewRenderService.RenderToStringAsync("Shared/_ShortPostPartial", post);
                renderedPosts.Add(rendered);
            }

            var model = new LoadMoreModel
            {
                Posts = renderedPosts,
                LastPostId = lastPostId
            };
            return Json(model);
        }
    }
}