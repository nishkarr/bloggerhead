using System;
using System.Collections.Generic;

namespace Bloggerhead.Models
{
    public class LoadMoreModel
    {
        public IEnumerable<string> Posts { get; set; }

        public int LastPostId { get; set; }
    }
}