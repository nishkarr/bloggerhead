﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Bloggerhead.Data
{
    public partial class bloggerheadContext : DbContext
    {
        public bloggerheadContext()
        {
        }

        public bloggerheadContext(DbContextOptions<bloggerheadContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Post> Post { get; set; }
        public virtual DbSet<PostTag> PostTag { get; set; }
        public virtual DbSet<Settings> Settings { get; set; }
        public virtual DbSet<Tag> Tag { get; set; }
        public virtual DbSet<User> User { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Post>(entity =>
            {
                entity.Property(e => e.Blurb).HasColumnType("text");

                entity.Property(e => e.Content).HasColumnType("text");

                entity.Property(e => e.ImgUrl)
                    .HasMaxLength(512)
                    .IsUnicode(false);

                entity.Property(e => e.PublishedDate).HasColumnType("datetime");

                entity.Property(e => e.Slug)
                    .HasMaxLength(64)
                    .IsUnicode(false);

                entity.Property(e => e.Title)
                    .HasMaxLength(512)
                    .IsUnicode(false);

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Post)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("FK__Post__UserId__3D5E1FD2");
            });

            modelBuilder.Entity<PostTag>(entity =>
            {
                entity.HasIndex(e => new { e.PostId, e.TagId })
                    .HasName("UQ_posttags")
                    .IsUnique();

                entity.HasOne(d => d.Post)
                    .WithMany(p => p.PostTag)
                    .HasForeignKey(d => d.PostId)
                    .HasConstraintName("FK__PostTag__PostId__3E52440B");

                entity.HasOne(d => d.Tag)
                    .WithMany(p => p.PostTag)
                    .HasForeignKey(d => d.TagId)
                    .HasConstraintName("FK__PostTag__TagId__3F466844");
            });

            modelBuilder.Entity<Settings>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.BackgroundImgUrl)
                    .IsRequired()
                    .HasMaxLength(512)
                    .IsUnicode(false);

                entity.Property(e => e.BlogDesc)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.BlogTitle)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Tag>(entity =>
            {
                entity.Property(e => e.Desc)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.Property(e => e.Email)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.FirstName)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Password).HasColumnType("text");

                entity.Property(e => e.Salt)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Surname)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.UserName)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });
        }
    }
}
