using System;
using System.Text;
using System.Linq;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Bloggerhead.Data;

namespace Bloggerhead.Services 
{
    public class PostService : IPostService
    {
        private bloggerheadContext dbContext;

        public PostService(bloggerheadContext dbCOntext)
        {
            this.dbContext = dbCOntext;
        }

        public Post CreatePost(string title, string content, string imgUrl, string status, User user, string[] tags, string blurb, string slug)
        {
            var post = new Post
            {
                Title = title,
                Content = content,
                ImgUrl = imgUrl,
                Published = status == "Published",
                User = user,
                Blurb = blurb,
                Slug = slug,
                PublishedDate = DateTime.UtcNow
            };

            dbContext.Post.Add(post);

            foreach(var tagDesc in tags)
            {
                var tag = dbContext.Tag.SingleOrDefault(t => t.Desc == tagDesc);
                if(tag == null)
                {
                    tag = new Tag
                    {
                        Desc = tagDesc
                    };
                    dbContext.Tag.Add(tag);
                }

                var postTag = new PostTag
                {
                    Post = post,
                    Tag = tag
                };

                dbContext.PostTag.Add(postTag);
            }

            dbContext.SaveChanges();
            return post;
        }

        public IEnumerable<Post> GetDraftPostsByDateDesc()
        {
            return dbContext.Post.Include(p => p.User)
                                 .Where(p => !p.Published)
                                 .AsEnumerable()
                                 .OrderByDescending(p => p.Id);
        }

        public Post GetPost(int id)
        {
            return dbContext.Post.Include(p => p.User)
                                 .Include(p => p.PostTag)
                                 .ThenInclude(pt => pt.Tag)
                                 .FirstOrDefault(p => p.Id == id);
        }

        public IEnumerable<Post> GetPostsByDateDesc()
        {
            return dbContext.Post.Include(p => p.User).AsEnumerable().OrderByDescending(p => p.Id);
        }

        public IEnumerable<Post> GetPublishedPostsByDateDesc(int beforeId = 100000, int limit = 2)
        {
            return dbContext.Post.Include(p => p.User)
                                 .Include(p => p.PostTag)
                                 .ThenInclude(pt => pt.Tag)
                                 .Where(p => p.Published && p.Id < beforeId)
                                 .OrderByDescending(p => p.Id)
                                 .Take(limit)
                                 .AsEnumerable();
        }

        public IEnumerable<string> GetTags(Post post)
        {
            return dbContext.PostTag.Where(pt => pt.Post == post).Select(pt => pt.Tag.Desc);
        }

        public void SavePost(Post post)
        {
            dbContext.Update(post);
            dbContext.SaveChanges();
        }
    }
}