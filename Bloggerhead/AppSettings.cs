using System;
using System.Collections.Generic;

namespace Bloggerhead
{
    public class AppSettings
    {
        public string AzureStorageAccount { get; set; }
        public string AzureStorageContainer { get; set; }
        public string AzureStorageAccountKey { get; set; }
    }
}