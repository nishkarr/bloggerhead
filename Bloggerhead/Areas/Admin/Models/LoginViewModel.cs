using System;
using System.ComponentModel.DataAnnotations;

namespace Bloggerhead.Admin.Models
{
    public class LoginViewModel
    {
        [Required]
        [StringLength(100)]
        public string UserName { get; set; }

        [Required]
        [StringLength(100)]
        public string Password { get; set; }
    }
}