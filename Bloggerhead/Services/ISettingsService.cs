using System;
using Bloggerhead.Data;

namespace Bloggerhead.Services
{
    public interface ISettingsService 
    {
        Settings GetSettings();

        void SaveSettings(Settings settings);
    }
}