using System;
using System.Collections.Generic;
using Bloggerhead.Data;

namespace Bloggerhead.Models
{
    public class FrontPageModel
    {
        public Settings Settings { get; set; }
        public IEnumerable<Post> Posts { get; set; }
    }
}