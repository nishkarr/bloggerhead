using System;
using Bloggerhead.Data;

namespace Bloggerhead.Services
{
    public interface IUserService 
    {
        User GetUser(string username);

        void SaveUser(User user);

        User AuthenticateUser(string username, string password);
    }
}

