using System;
using Bloggerhead.Data;

namespace Bloggerhead.Models
{
    public class ViewPostModel
    {
        public Settings Settings { get; set; }
        public Post Post { get; set; }
    }
}