create database bloggerhead
go

use bloggerhead
go

 --drop table Settings
 --go 
 --drop table [User]
 --go
 --drop table [Post]
 --go
 --drop table Tag
 --go
 --drop table PostTag
 --go

create table Settings ( -- haha table settings ;)
    Id int PRIMARY KEY not null,
    BlogTitle varchar(100) not null,
    BlogDesc varchar(255) not null,
    BackgroundImgUrl varchar(512) not null,
    InitialPostCount int not null,
    LoadMorePostsCount int not null
)
go

create table [User] (
    Id int IDENTITY PRIMARY KEY not null,
	FirstName varchar(100) not null, 
	Surname varchar(100) not null,
    UserName varchar(100) not null,
    Password text,
    Salt varchar(100),
    Email varchar(255)
)
go

create table Post (
    Id int IDENTITY PRIMARY KEY not null,
    Title varchar(512),
    Content text,
    ImgUrl varchar(512),
    UserId int FOREIGN KEY REFERENCES [User](Id),
	Published bit not null,
	PublishedDate datetime,
	Blurb text,
	Slug varchar(64)
)
go 

create table Tag (
    Id int IDENTITY PRIMARY KEY not null,
    [Desc] varchar(100) not null
)
go 

create table PostTag (
    Id int IDENTITY PRIMARY KEY not null,
    PostId int FOREIGN KEY REFERENCES Post(Id),
    TagId int FOREIGN KEY REFERENCES Tag(Id)
    CONSTRAINT [UQ_posttags] UNIQUE NONCLUSTERED
    (
        PostId, TagId
    )
)
go