﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Bloggerhead.Models;
using Bloggerhead.Services;

namespace Bloggerhead.Controllers
{
    public class HomeController : Controller
    {
        private IPostService postService;
        private ISettingsService settingsService;

        public HomeController(IPostService postService, ISettingsService settingsService)
        {
            this.postService = postService;
            this.settingsService = settingsService;
        }

        public IActionResult Index()
        {
            var settings = settingsService.GetSettings();

            var model = new FrontPageModel
            {
                Settings = settings,
                Posts = postService.GetPublishedPostsByDateDesc(limit: settings.InitialPostCount)
            };

            return View(model);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
