﻿using System;
using System.Collections.Generic;

namespace Bloggerhead.Data
{
    public partial class Settings
    {
        public int Id { get; set; }
        public string BlogTitle { get; set; }
        public string BlogDesc { get; set; }
        public string BackgroundImgUrl { get; set; }
        public int InitialPostCount { get; set; }
        public int LoadMorePostsCount { get; set; }
    }
}
