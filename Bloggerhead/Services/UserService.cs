using System;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using Bloggerhead.Data;

namespace Bloggerhead.Services 
{
    public class UserService : IUserService
    {
        private bloggerheadContext dbContext;

        public UserService(bloggerheadContext dbCOntext)
        {
            this.dbContext = dbCOntext;
        }
        public User GetUser(string username) => dbContext.User.FirstOrDefault(x => x.UserName == username);

        public void SaveUser(User user)
        {
            dbContext.User.Update(user);
            dbContext.SaveChanges();
        }
        
        public User AuthenticateUser(string username, string password)
        {
            var user = GetUser(username);

            if(user == null)
                return null;
            
            var hashedPwd = createSaltedHash(user.Salt, password);

            Console.WriteLine(hashedPwd);

            if(hashedPwd == user.Password)
            {
                return user;
            }
            return null;
        }

        private static string createSaltedHash(string salt, string password)
        {
            using(var sha256 = SHA256.Create())  
            {  
                var hashedBytes = sha256.ComputeHash(Encoding.UTF8.GetBytes($"{salt}{password}"));  
                return BitConverter.ToString(hashedBytes).Replace("-", "").ToLower();
            }  
        }

        private static string generateSalt()
        {
            byte[] salt = new byte[128 / 8];
            using (var rng = RandomNumberGenerator.Create())
            {
                rng.GetBytes(salt);
            }
            return Convert.ToBase64String(salt);
        } 

    }
}