using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Security.Cryptography;
using System.Net;
using Microsoft.Extensions.Options;

namespace Bloggerhead.Admin.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AzureAuthController : Controller
    {
        private AppSettings appSettings;

        public AzureAuthController(IOptions<AppSettings> options)
        {
            this.appSettings = options.Value;
        }

        [HttpGet("{filename}", Name = "GetAuthSignature")]
        public ActionResult<AzureAuth> GetAuthSignature(string filename)
        {
            if(filename.IndexOf(".") < 0)
            {
                return new AzureAuth { Error = "invalid filename." };
            }

            var storageAccount = appSettings.AzureStorageAccount;
            var storageAccountKey = appSettings.AzureStorageAccountKey;
            var containerName = appSettings.AzureStorageContainer;

            var contentType = determineContentType(filename);
            var date = DateTime.Now.ToUniversalTime().ToString("R");;
            var objectPath = $"/{storageAccount}/{containerName}/{Uri.EscapeDataString(filename)}";

            var headerStr = $"PUT\n\n{contentType}\n\nx-ms-date:{date}\nx-ms-meta-m1:v1\nx-ms-meta-m2:v2\n{objectPath}";
            var headerBytes = Encoding.UTF8.GetBytes(headerStr);

            var authHeader = "";
            using (HMACSHA256 hash = new HMACSHA256(Convert.FromBase64String(storageAccountKey)))
            {
                authHeader = Convert.ToBase64String(hash.ComputeHash(headerBytes));
            }

            var azureAuth = new AzureAuth();
            azureAuth.AuthHeaders = new List<AuthHeader>{
                new AuthHeader { Name = "Authorization", Value = $"SharedKey {storageAccount}:{authHeader}" },
                new AuthHeader { Name = "Content-Type", Value =  contentType },
                new AuthHeader { Name = "x-ms-date", Value = date },
                new AuthHeader { Name = "x-ms-meta-m1", Value = "v1" },
                new AuthHeader { Name = "x-ms-meta-m2", Value = "v2" },
            };
            return azureAuth;
        }

        private static string determineContentType(string filename)
        {
            var extension = filename.ToLower().Substring(filename.LastIndexOf("."));
            switch(extension)
            {
                case ".jpg":
                case ".jpeg":
                    return "image/jpeg";
                case ".png":
                    return "image/png";
                default:
                    return "application/octet-stream";
            }
        }
    }

    public class AuthHeader 
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }

    public class AzureAuth
    {
        public string Error { get; set; }
        public List<AuthHeader> AuthHeaders { get; set; }
    }

}