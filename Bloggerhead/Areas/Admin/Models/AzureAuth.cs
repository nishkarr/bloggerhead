using System;
using System.Collections.Generic;

namespace Bloggerhead.Admin.Models
{
    public class AzureAuth
    {
        public string Error { get; set; }
        public List<AuthHeader> AuthHeaders { get; set; }
    }
}