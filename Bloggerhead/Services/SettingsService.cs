using System;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using Bloggerhead.Data;

namespace Bloggerhead.Services 
{
    public class SettingsService : ISettingsService
    {
        private bloggerheadContext dbContext;

        const int SETTINGS_ID = 1;

        public SettingsService(bloggerheadContext dbCOntext)
        {
            this.dbContext = dbCOntext;
        }

        public Settings GetSettings()
        {
            return dbContext.Settings.Find(SETTINGS_ID);
        }

        public void SaveSettings(Settings settings)
        {
            dbContext.Update(settings);
            dbContext.SaveChanges();
        }
    }
}